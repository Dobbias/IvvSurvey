var express = require('express');
var ParseDashboard = require('parse-dashboard');

var runsLocally = true;
var port = runsLocally ? 1338 : 80;
var options = { allowInsecureHTTP: true };

var apps = [
  {
    "serverURL": runsLocally ? "http://127.0.0.1:1337/parse"
      : "https://parse.MYDOMAIN.COM/parse",
    "appId": "",
    "masterKey": "",
    "appName": "IVV Website"
  }
];

var config = {};

if (runsLocally) {
  config = {
    "apps": apps,
    "useEncryptedPasswords": true,
    "trustProxy": 1
  };
} else {
  config = {
    "apps": apps,
    "users": [
      {
        "user": "admin",
        // same as mongodb password
        "pass": "BCRYPT_PW"
      }
    ],
    "useEncryptedPasswords": true,
    "trustProxy": 1
  };
}

var dashboard = new ParseDashboard(config, options);

var app = express();

app.use('/', dashboard);

var httpServer = require('http').createServer(app);
httpServer.listen(port, runsLocally ? "127.0.0.1" : "0.0.0.0", function () {
  console.log('parse-dashboard running on port ' + port + '.');
});
