const Participant = Parse.Object.extend("Participant");
const LoggingEvent = Parse.Object.extend("LoggingEvent");
const Task = Parse.Object.extend("Task");
const TaskResult = Parse.Object.extend("TaskResult");

// Höchster Bildungabschluss, Job not required
// Probant 4 Tasks 2 mit time pressure 2 ohne alle random

//startExperiment returns participant id number *
Parse.Cloud.define("startExperiment", async (request) => {
    const participant = new Participant();

    participant.set("Step", 0);

    var acl = new Parse.ACL();
    participant.setACL(acl);

    return (await participant.save(null, { useMasterKey: true })).id;
});

// Demographic*
Parse.Cloud.define("submitDemographic", async (request) => {
    var participant = (await new Parse.Query("Participant")
        .equalTo("objectId", request.params.participantId)
        .find({ useMasterKey: true }))[0];

    if (participant.get("Step") !== 0)
        return false;

    participant.set("Age", request.params.age);
    participant.set("Gender", request.params.gender);
    participant.set("Occupation", request.params.occupation);
    participant.set("Education", request.params.education);
    participant.set("Step", 1);

    await participant.save(null, { useMasterKey: true });

    return true;
});

// Log: Suchanfrage Inhalt, Auf und zuklicken von Odnern und anklicken von docs,  Wo wurde doc angeklickt in suchergebnissen oder baum

// Richtig wenn Document geöffnet und irgendeine Antwort eingeben

//Timepressure wenn Zeit um -> vorbei

//client user has to create structure done --> Save Online

Parse.Cloud.define("getTask", async (request) => {
    var participant = (await new Parse.Query("Participant")
        .equalTo("objectId", request.params.participantId)
        .find({ useMasterKey: true }))[0];

    var query = new Parse.Query(Task);
    var tasks = await query.find({ useMasterKey: true });

    // Shuffle tasks
    for (let i = tasks.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [tasks[i], tasks[j]] = [tasks[j], tasks[i]];
    }

    var query2 = new Parse.Query(TaskResult);
    query2.equalTo("Owner", participant);
    var taskResults = await query2.find({ useMasterKey: true });

    var task = null;

    for (i = 0; i < tasks.length; i++) {
        var t = tasks[i];

        var used = false;
        for (j = 0; j < taskResults.length; j++) {
            var tr = taskResults[j];
            if (tr.get("Task").id == t.id) {
                used = true;
                break;
            }
        }
        if (!used) {
            task = t;
            break;
        }
    }

    const taskResult = new TaskResult();
    taskResult.set("Owner", participant);
    taskResult.set("Task", task);
    await taskResult.save(null, { useMasterKey: true });

    participant.set("CurrentTask", task);
    participant.set("CurrentTaskResult", taskResult);
    await participant.save(null, { useMasterKey: true });

    return {
        document: task.get("Document").id, infoText: task.get("InfoText"),
        taskName: task.get("TaskName"), timePressure: task.get("TimePressure"),
        timeLimit: task.get("TimeLimit"), answer: task.get("Answer")
    };
});

//TaskInfo -> start
Parse.Cloud.define("logData", async (request) => {
    var participant = (await new Parse.Query("Participant")
        .equalTo("objectId", request.params.participantId)
        .find({ useMasterKey: true }))[0];

    var taskResult = participant.get("CurrentTaskResult");

    if (taskResult === undefined) {
        if (participant.get("Step") !== 1) {
            return false;
        } else {
            participant.set("Step", 2);
            await participant.save(null, { useMasterKey: true });
        }
    }

    const loggingEvent = new LoggingEvent();
    loggingEvent.set("Data", request.params.data);
    loggingEvent.set("TaskResult", taskResult);
    loggingEvent.set("Participant", participant);
    loggingEvent.set("Time", new Date());
    var acl = new Parse.ACL();
    loggingEvent.setACL(acl);
    await loggingEvent.save(null, { useMasterKey: true });
    return true;
});

// End when found answered

// End Task

// Log duration

// GetTask Until zero 

// Erklärung
// Notiz
// Thankyou pg enter email

Parse.Cloud.define("submitEmail", async (request) => {
    var participant = (await new Parse.Query("Participant")
        .equalTo("objectId", request.params.participantId)
        .find({ useMasterKey: true }))[0];

    if (participant.get("Step") !== 2)
        return false;

    participant.set("Step", 3);
    participant.set("EMail", request.params.email);
    participant.set("UrHours", request.params.urHours);

    await participant.save(null, { useMasterKey: true });

    return true;
});
