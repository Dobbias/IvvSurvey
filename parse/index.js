var runsLocally = true;
var port = runsLocally ? 1337 : 80;

var express = require('express');
var ParseServer = require('parse-server').ParseServer;


var api = new ParseServer({
  databaseURI: 'mongodb://USERNAME:PASSWORD@SERVEe:57834/DATABASENAME',
  cloud: __dirname + '/cloud/main.js',
  appId: '',
  masterKey: '',
  serverURL: `http://127.0.0.1:${port}/parse`,
  allowClientClassCreation: false,
  enableAnonymousUsers: true,
  logLevel: 'error',
  sessionLength: 172800,
  passwordPolicy: {
    validatorPattern: /^(?=.{6,})/
  }
});

var app = express();

app.use(function (req, res, next) {
  if (req.path.substring(0, 12) === '/parse/files') {
    res.status(400).send({ code: 119, message: 'files endpoints are disabled' });
    return;
  }
  next();
});

// make the Parse Server available at /parse
app.use('/parse', api);

var httpServer = require('http').createServer(app);
httpServer.listen(port, runsLocally ? "127.0.0.1" : "0.0.0.0", function () {
  console.log('parse-server-example running on port ' + port + '.');
});
