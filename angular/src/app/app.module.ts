import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { TreeModule } from 'ng2-tree';
import { StorageServiceModule } from 'ngx-webstorage-service';

import { MaterialModule } from './material/material.module'

import { AppComponent } from './app.component';
import { LandingComponent } from './pages/landing/landing.component';
import { DemographicsComponent } from './pages/demographics/demographics.component';
import { PresortComponent } from './pages/presort/presort.component';
import { PretasksComponent } from './pages/pretasks/pretasks.component';
import { PretaskComponent } from './pages/pretask/pretask.component';
import { TaskComponent } from './pages/task/task.component';
import { SortComponent } from './pages/sort/sort.component';
import { EndComponent } from './pages/end/end.component';

import { DocumentDialogComponent } from './dialogs/document-dialog.component';

import { AuthGuardService as AuthGuard } from './services/auth-guard.service';

const appRoutes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'demographics', component: DemographicsComponent },
  { path: 'presort', component: PresortComponent },
  { path: 'pretasks', component: PretasksComponent },
  { path: 'pretask', component: PretaskComponent },
  { path: 'task', component: TaskComponent },
  { path: 'sort', component: SortComponent },
  { path: 'end', component: EndComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    DemographicsComponent,
    PresortComponent,
    PretasksComponent,
    PretaskComponent,
    TaskComponent,
    SortComponent,
    EndComponent,
    DocumentDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    TreeModule,
    StorageServiceModule,
    RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' })
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [DocumentDialogComponent]
})
export class AppModule { }
