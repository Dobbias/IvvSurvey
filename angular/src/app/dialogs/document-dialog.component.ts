import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
    selector: 'document-dialog',
    templateUrl: './document-dialog.component.html'
})
export class DocumentDialogComponent {

    constructor(
        public dialogRef: MatDialogRef<DocumentDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }
}