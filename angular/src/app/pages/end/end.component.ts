import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ParseService } from '../../services/parse.service';
import { MyStorageService } from '../../services/mystorage.service';

@Component({
  selector: 'end',
  templateUrl: './end.component.html',
  styleUrls: ['./end.component.scss']
})
export class EndComponent implements OnInit {
  signupForm: FormGroup;

  constructor(private parseService: ParseService, private storageService: MyStorageService,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      'ur_h': new FormControl('', [
      ])
    });
  }

  async send() {
    if (this.signupForm.valid) {
      let step: number = this.storageService.getObject("step") || 0;
      if (step != 3) {
        return;
      }
      let participandId: string = this.storageService.getObject("participantId") || "";
      if (participandId != "") {
        await this.parseService.SubmitEmail(participandId, this.signupForm.get("email").value,
          this.signupForm.get("ur_h").value || false);
        this.storageService.setObject("step", 4);
        this.storageService.clear();
        this.snackBar.open("Erfolgreich. Du kannst die Seite schließen.")
      }
    }
  }
}