import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'presort',
  templateUrl: './presort.component.html',
  styleUrls: ['./presort.component.scss']
})
export class PresortComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onStart() {
    this.router.navigate(['sort']);
  }
}
