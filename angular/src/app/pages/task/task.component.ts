import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';

import { MyStorageService } from '../../services/mystorage.service';
import { ParseService } from '../../services/parse.service';

import { TreeModel, NodeEvent } from 'ng2-tree';

import { DocumentDialogComponent } from '../../dialogs/document-dialog.component'
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit, OnDestroy {

  readonly TASK_COUNT = 4;
  documents: any[];

  dialogRef: MatDialogRef<DocumentDialogComponent, any> = null;

  lastDocument: string = null;
  answer: string;
  participantId: string;

  searchForm: FormGroup;
  searchResults = [];
  answerForm: FormGroup;

  currentTask: any;

  TaskTitle: string = "Aufgabe ";
  TaskText: string = "Laden ...";

  TimePressure: boolean = false;
  TimeLimit: string = "0m 0s";
  TimeLeft: number = 0;
  Subscription: Subscription = null;

  public initialTree: TreeModel = {
    settings: {
      rightMenu: false,
      static: true,
      isCollapsedOnInit: true,
    },
    value: 'Mein Ordner',
    children: []
  };

  constructor(private router: Router, private parseService: ParseService,
    private storageService: MyStorageService, public dialog: MatDialog,
    public snackBar: MatSnackBar) { }

  async ngOnInit() {
    this.currentTask = this.storageService.getObject("taskObject");
    const task = this.storageService.getObject("task");

    this.TaskTitle = "Aufgabe " + (task + 1);
    this.TaskText = this.currentTask.infoText;
    this.TimeLeft = this.currentTask.timeLimit;
    this.answer = (this.currentTask.answer as string).toLowerCase();

    console.log(this.answer);

    const seconds = this.TimeLeft % 60;
    const minutes = (this.TimeLeft - seconds) / 60;
    this.TimeLimit = `${minutes}m ${seconds}s`;
    this.TimePressure = this.currentTask.timePressure;

    this.initialTree.children = this.storageService.getObject("tree");
    this.documents = this.storageService.getObject("documents");

    this.searchForm = new FormGroup({
      'searchBox': new FormControl('')
    });

    this.answerForm = new FormGroup({
      'answerBox': new FormControl('')
    });

    this.participantId = this.storageService.getObject("participantId");
    await this.parseService.LogData(this.participantId, "st: " + task);

    if (this.TimePressure) {
      let timer = Observable.timer(0, 1000);
      this.Subscription = timer.subscribe(async t => {
        this.TimeLeft--;
        const seconds = this.TimeLeft % 60;
        const minutes = (this.TimeLeft - seconds) / 60;
        this.TimeLimit = `${minutes}m ${seconds}s`;

        if (this.TimeLeft <= 0) {
          await this.parseService.LogData(this.participantId, "timeout");
          this.onEnd();
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.Subscription != null) {
      this.Subscription.unsubscribe();
    }
  }

  search(query: string): Array<any> {
    let result = new Array<any>();
    this.documents.forEach(document => {
      if ((document.Text as string).toLowerCase().includes(query.toLowerCase())) {
        result.push(document);
      }
    });
    return result;
  }

  async onEnd() {
    if (this.Subscription != null) {
      this.Subscription.unsubscribe();
    }
    if (this.dialogRef != null) {
      this.dialogRef.close();
    }
    const task = this.storageService.getObject("task");
    await this.parseService.LogData(this.participantId, "et: " + task);
    if (task + 1 == this.TASK_COUNT) {
      let step: number = this.storageService.getObject("step") || 0;
      if (step != 2) {
        return;
      }
      this.storageService.setObject("step", 3);
      this.router.navigate(['end']);
    } else {
      this.storageService.setObject("task", task + 1);
      this.router.navigate(['pretask']);
    }
  }

  async onSearch() {
    this.searchResults = [];
    const query = this.searchForm.get("searchBox").value as string;
    await this.parseService.LogData(this.participantId, "s: " + query);
    if (query.length == 0)
      return;
    var docs = this.search(query);
    docs.forEach(document => {
      this.searchResults.push({ title: document.Title, id: document.objectId });
    });
  }

  async onAnswer() {
    const ans = this.answerForm.get("answerBox").value as string;

    if (ans.toLowerCase() === this.answer || this.lastDocument === this.currentTask.document) {
      await this.parseService.LogData(this.participantId, "a: " + ans);
      this.onEnd();
    } else {
      await this.parseService.LogData(this.participantId, "wa: " + ans);
      this.snackBar.open("Falsche Antwort. Probiere es weiter!", null, {
        duration: 6000,
      });
    }
  }

  public async onNodeSelected(e: NodeEvent) {
    if (!e.node.hasChildren()) {
      this.showDocument(e.node.id as string);
      await this.parseService.LogData(this.participantId, "ctn: " + e.node.id);
    }
  }

  public async onNodeExpanded(e: NodeEvent) {
    await this.parseService.LogData(this.participantId, "exp: " + e.node.id);
  }

  public async onNodeCollapsed(e: NodeEvent) {
    await this.parseService.LogData(this.participantId, "coll: " + e.node.id);
  }

  async showDocumentFromSearch(id: string) {
    this.showDocument(id);
    await this.parseService.LogData(this.participantId, "csr: " + id);
  }

  showDocument(id: string) {
    let doc = null;
    for (let document of this.documents) {
      if (document.objectId === id) {
        doc = document;
        break;
      }
    }

    if (doc !== null) {
      this.lastDocument = doc.objectId;
      this.dialogRef = this.dialog.open(DocumentDialogComponent, {
        width: '250px',
        data: { title: doc.Title, text: doc.Text },
        height: "80%",
        maxWidth: "60%",
        minWidth: "60%"
      });

      this.dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
        this.dialogRef = null;
      });
    }
  }
}
