import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'pretasks',
  templateUrl: './pretasks.component.html',
  styleUrls: ['./pretasks.component.scss']
})
export class PretasksComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onStart() {
    this.router.navigate(['pretask']);
  }
}
