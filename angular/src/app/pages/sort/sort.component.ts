import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  TreeModel, Ng2TreeSettings, NodeMenuItemAction, MenuItemSelectedEvent,
  NodeEvent, NodeMovedEvent, Tree
} from 'ng2-tree';
import { MatSnackBar, MatDialog } from '@angular/material'

import { ParseService } from '../../services/parse.service';
import { MyStorageService } from '../../services/mystorage.service';

import { DocumentDialogComponent } from '../../dialogs/document-dialog.component'

@Component({
  selector: 'sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.scss']
})
export class SortComponent implements OnInit {

  public settings: Ng2TreeSettings = {
    rootIsVisible: false
  };

  @ViewChild('treeDestination') public treeDestination;

  documents: any[];
  buttonStatus = true;

  public initialTree: TreeModel = {
    settings: {
      rightMenu: false,
    },
    value: 'Start - Ordner',
    children: [{ value: 'Arial', id: 12 }]
  };

  public tree: TreeModel = {
    settings: {
      menuItems: [
        { action: NodeMenuItemAction.NewFolder, name: 'Neuer Ordner', cssClass: '' },
        { action: NodeMenuItemAction.Custom, name: 'Löschen', cssClass: '' },
        { action: NodeMenuItemAction.Rename, name: 'Umbenennen', cssClass: '' },
      ]
    },
    value: 'Mein Ordner',
    children: []
  };

  constructor(private router: Router, private parseService: ParseService,
    private storageService: MyStorageService, public snackBar: MatSnackBar,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.documents = this.storageService.getObject("documents");
    let data = new Array<TreeModel>();
    this.documents.forEach(document => {
      data.push({ value: document.Title, id: document.objectId });
    });
    this.initialTree.children = data;
  }

  async onStart() {
    let participandId: string = this.storageService.getObject("participantId") || "";

    const model = (this.treeDestination.tree as Tree).toTreeModel();
    let step: number = this.storageService.getObject("step") || 0;
    if (step != 1) {
      return;
    }
    this.storageService.setObject("step", 2);
    let validKeys = ['children', 'id', 'value'];
    this.deleteInvalidKeys(model, validKeys);
    this.storageService.setObject("tree", model.children);
    await this.parseService.LogData(participandId, JSON.stringify(model.children));
    this.router.navigate(['pretasks']);
  }

  public deleteInvalidKeys(obj: any, validKeys: Array<string>) {
    Object.keys(obj).forEach((key) => validKeys.includes(key) || delete obj[key]);
    if (obj.hasOwnProperty("children")) {
      let children: Array<any> = obj.children;
      children.forEach(child => {
        this.deleteInvalidKeys(child, validKeys);
      });
    }
  }

  public onMenuItemSelected(e: MenuItemSelectedEvent) {
    if (!e.node.hasChildren()) {
      e.node.removeItselfFromParent();
    } else {
      this.snackBar.open("Nur leere Ordner können gelöscht werden.", null, {
        duration: 2000,
      });
    }
  }

  onNodeMoved(e: NodeMovedEvent) {
  }

  showDocument(id: string) {
    let doc = null;
    for (let document of this.documents) {
      if (document.objectId === id) {
        doc = document;
        break;
      }
    }

    if (doc !== null) {
      const dialogRef = this.dialog.open(DocumentDialogComponent, {
        width: '250px',
        data: { title: doc.Title, text: doc.Text },
        height: "80%",
        maxWidth: "60%",
        minWidth: "60%"
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
      });
    }
  }

  public onNodeRemoved(e: NodeEvent): void {
    if (e.node.parent.value == this.initialTree.value) {
      this.buttonStatus = e.node.parent.hasChildren();
    }
  }

  public onNodeSelected(e: NodeEvent): void {
    if (!e.node.hasChildren()) {
      this.showDocument(e.node.id as string);
    }
  }
}
