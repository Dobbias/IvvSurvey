import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ParseService } from '../../services/parse.service';
import { MyStorageService } from '../../services/mystorage.service';

@Component({
  selector: 'pretasks',
  templateUrl: './pretask.component.html',
  styleUrls: ['./pretask.component.scss']
})
export class PretaskComponent implements OnInit {

  TaskTitle: string = "Aufgabe ";
  TaskText: string = "Laden ...";

  TimePressure: boolean = false;
  TimeLimit: string = "0m 0s";

  constructor(private router: Router, private parseService: ParseService,
    private storageService: MyStorageService) { }

  async ngOnInit() {
    const task = this.storageService.getObject("task");
    const taskFinished = this.storageService.getObject("taskFinished");

    this.TaskTitle = "Aufgabe " + (task + 1);

    var currentTask = null;

    if (taskFinished + 2 == task) {
      currentTask = await this.parseService.GetTask(
        this.storageService.getObject("participantId"));
      this.storageService.setObject("taskObject", currentTask);
      this.storageService.setObject("taskFinished", taskFinished + 1);
    } else {
      currentTask = this.storageService.getObject("taskObject");
    }

    this.TaskText = currentTask.infoText;
    const seconds = currentTask.timeLimit % 60;
    const minutes = (currentTask.timeLimit - seconds) / 60;
    this.TimeLimit = `${ minutes }m ${ seconds }s`;
    this.TimePressure = currentTask.timePressure;
  }

  onStart() {
    this.router.navigate(['task']);
  }
}
