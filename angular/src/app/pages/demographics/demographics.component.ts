import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ParseService } from '../../services/parse.service';
import { MyStorageService } from '../../services/mystorage.service';
import { IvvDocument } from 'src/app/classes/Document';

@Component({
  selector: 'demographics',
  templateUrl: './demographics.component.html',
  styleUrls: ['./demographics.component.scss']
})
export class DemographicsComponent implements OnInit {

  hide = true;
  signupForm: FormGroup;

  constructor(private router: Router, private parseService: ParseService,
    private storageService: MyStorageService, public snackBar: MatSnackBar) { }

  async ngOnInit() {
    this.signupForm = new FormGroup({
      'occupation': new FormControl('', [
      ]),
      'education': new FormControl('', [
      ]),
      'gender': new FormControl('', [
        Validators.required,
      ]),
      'age': new FormControl('', [
        Validators.required,
        Validators.maxLength(3)
      ])
    });

    let step: number = this.storageService.getObject("step") || 0;
    if (step == 3) {
      this.storageService.clear();
    }

    let documents: IvvDocument[] = this.storageService.getObject("documents");

    if (documents == null) {
      documents = await this.parseService.GetDocuments();
      this.storageService.setObject("documents", documents);
    }
  }

  async signup() {
    if (this.signupForm.valid) {
      let step: number = this.storageService.getObject("step") || 0;
      if (step != 0) {
        return;
      }
      let participandId: string = this.storageService.getObject("participantId") || "";
      if (participandId == "") {
        participandId = await this.parseService.StartExperiment();
        this.storageService.setObject("participantId", participandId);
      }
      await this.parseService.SubmitDemographic(
        participandId, this.signupForm.get("gender").value,
        this.signupForm.get("occupation").value, this.signupForm.get("education").value,
        this.signupForm.get("age").value);
      this.storageService.setObject("step", 1);
      this.storageService.setObject("task", 0);
      this.storageService.setObject("taskFinished", -2);
      this.router.navigate(['presort']);
    }
  }
}
