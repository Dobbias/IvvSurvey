import { Injectable } from '@angular/core';
import {
    CanActivate,
    CanActivateChild,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { ParseService } from './parse.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {
    constructor(private parseService: ParseService, private router: Router) {}

    canActivate() {
        return true;
    }

    canActivateChild(): boolean {
        return this.canActivate();
    }
}
