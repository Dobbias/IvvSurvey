import { Injectable, isDevMode } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { IvvDocument } from '../classes/Document';

import * as Parse from 'parse';

@Injectable({
    providedIn: 'root',
})
export class ParseService {

    private parseAppId: string = "";
    private parseServerUrl: string = environment.parseServerUrl;

    private user: Parse.User;

    constructor() {
        Parse.initialize(this.parseAppId);
        (Parse as any).serverURL = this.parseServerUrl;
        Parse.Object.registerSubclass('Document', IvvDocument);
    }

    public async StartExperiment(): Promise<string> {
        return await Parse.Cloud.run("startExperiment");
    }

    public async LogData(participantId: string, data: string): Promise<string> {
        return await Parse.Cloud.run("logData", { participantId: participantId, data: data });
    }

    public async SubmitDemographic(participantId: string, gender: string, occupation: string,
        education: string, age: number): Promise<string> {
        return await Parse.Cloud.run("submitDemographic", {
            participantId: participantId, gender: gender, occupation: occupation,
            education: education, age: age
        });
    }

    public async SubmitEmail(participantId: string, email: string, urHours: boolean): Promise<string> {
        return await Parse.Cloud.run("submitEmail", {
            participantId: participantId, email: email, urHours: urHours
        });
    }

    public async GetTask(participantId: string): Promise<any> {
        return await Parse.Cloud.run("getTask", { participantId: participantId });
    }

    public async GetDocuments(): Promise<IvvDocument[]> {
        var query = new Parse.Query(IvvDocument);
        return await query.find();
    }
}
