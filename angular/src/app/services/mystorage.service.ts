import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

const STORAGE_KEY = 'ivv-ur-website';

@Injectable({
    providedIn: 'root',
})
export class MyStorageService {

    constructor(@Inject(SESSION_STORAGE) private storage: StorageService) {

    }

    public setObject(key: string, value: any) {
        this.storage.set(STORAGE_KEY + key, value);
    }

    public getObject(key: string) {
        return this.storage.get(STORAGE_KEY + key);
    }

    public clear() {
        this.storage.clear();
    }
}
